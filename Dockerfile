FROM golang
WORKDIR /home/app

# --- install dependencies, then run the application
CMD /bin/bash -c "./install.dependencies.sh; go build main.go; ./main"
